/**
 * Created by Steve on 11/24/2015.
 */
var MrMeeseeks = angular.module('mrmeeseeks', ['ui.router']);

MrMeeseeks.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    //
    // For any unmatched url, redirect to /home
    $urlRouterProvider.otherwise("/");

    $locationProvider.html5Mode(true);
    
    //
    // Now set up the states
    $stateProvider
        .state('home', {
            url: "/",
            templateUrl: "partials/home.html",
            controller: function($scope) {
                var loadedSounds = {};
                
                $scope.sounds = [
                    {
                        'id':'ImMrMeeseeks',
                        'text':'I\'m Mr.Meeseeks!'
                    },
                    {
                        'id':'HeyThereImMrMeeseeks',
                        'text':'I\'m Mr.Meeseeks! (2)'
                    },
                    {
                        'id':'Poof_ImMrMeeseeksLookAtMe',
                        'text':'Look At Me!'
                    },
                    {
                        'id':'ImMrMeeseeks_SoIcanGoAway',
                        'text':'My Purpose!'
                    },
                    {
                        'id':'AllDone',
                        'text':'All Done!'
                    },
                    {
                        'id':'ImABitOfASticklerMeeseeks',
                        'text':'I\'m A Bit Of A Stickler...'
                    },
                    {
                        'id':'ItsGettinWeird',
                        'text':'It\'s Gettin Weird!'
                    },
                    {
                        'id':'IJustWannaDie',
                        'text':'I Just Wanna Die!'
                    },
                    {
                        'id':'OhhHesTryin',
                        'text':'Ohh He\'s Tryin!'
                    },
                    {
                        'id':'OhhYeaaYesMaham',
                        'text':'Yes Maham!'
                    },
                    {
                        'id':'OhhYeaCanDo',
                        'text':'Can Do!'
                    },
                    {
                        'id':'ThatsOkay',
                        'text':'That\s Okay!'
                    },
                    {
                        'id':'TryAgainAndKeepYourHeadDown',
                        'text':'Keep Your Head Down!'
                    },
                    {
                        'id':'WhoaOkay',
                        'text':'Whoa! Okay!'
                    },
                    {
                        'id':'Yessiree',
                        'text':'Yessiree!'
                    },
                    {
                        'id':'YouGottaRelax',
                        'text':'You Gotta Relax!'
                    },
                    {
                        'id':'YourFailuresAreYourOwnOldMan',
                        'text':'Your Failures...'
                    },
                    {
                        'id':'HavingAFamilyMonologue',
                        'text':'Family Monologue'
                    },
                    {
                        'id':'Poof',
                        'text':'Poof!'
                    },
                ];
                
                $scope.playSound = function(id, $event){
                    if(loadedSounds[id]){
                        loadedSounds[id].play();
                        loadedSounds[id].currentTime=0;
                    }
                    else{
                        loadedSounds[id] = new Audio("audio/"+ id + ".mp3");
                        loadedSounds[id].play();
                        loadedSounds[id].currentTime=0;
                    }
                    
                    //blur the button
                    angular.element($event.target).blur();
                };
                
            }
        })
//         .state('map', {
//             url: "/map",
//             templateUrl: "partials/map.html",
//             controller: function($scope) {
//                 var map,
//                     mapPoint,
//                     maxZoomScale,
//                     pointGraphic,
//                     pictureMarkerSymbol,
//                     esriGraphic;
//                 require(["esri/map",
//                     "esri/geometry/Point",
//                     "esri/SpatialReference",
//                     "esri/symbols/PictureMarkerSymbol",
//                     "esri/graphic",
//                     "dojo/domReady!"], function (Map,
//                                                  Point,
//                                                  SpatialReference,
//                                                  PictureMarkerSymbol,
//                                                  Graphic) {
//                     esriGraphic = Graphic;
// 
//                     //start up map stuff
//                     map = new Map("mapDiv", {
//                         basemap: "hybrid"
//                     });
// 
//                     map.on("load", function (layer) {
//                         try {
//                             mapPoint = new Point(-74.0059, 40.7127);
//                             if (map.basemapLayerIds && map.basemapLayerIds.length > 0) {
//                                 var basemap = map.getLayer(map.basemapLayerIds[0]);
//                                 if (basemap.scales && basemap.scales.length > 4) {
//                                     maxZoomScale = basemap.scales.length - 3;
//                                     map.centerAndZoom(mapPoint, maxZoomScale);
//                                 }
//                             }
// 
//                             pictureMarkerSymbol = new PictureMarkerSymbol('img/mrmeeseeksIcon.gif', 51, 51);
//                             pointGraphic = new esriGraphic(mapPoint, pictureMarkerSymbol);
//                             map.graphics.add(pointGraphic);
//                         }
//                         catch (e) {
//                             console.log(e);
//                         }
//                     });
//                 });
//             }
//         });
})
.run(['$log', '$state', function ($log, $state) {
    //$state.go('home');
    $log.info('Successfully Booted MrMeeseeks');  
}]);