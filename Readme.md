### What is this? ###

This is a hilariously over engineered sound board app.  Basically its a responsive web app that allows you to push buttons and hear the catch phrases of the character "Mr.Meeseeks" from the TV show Rick and Morty.  This could have been very easily accomplished with vanilla html and javascript...but that wasn't the point.  So why make this?

### Benefits 
* Build a boilerplate angular project that I could use for other SPA applications in the future
* Have some angular code that I actually own for my resume
* Practice with grunt and other build tools
* Practice with AWS and Cloud Hosting Tools

### Hosting ###
This app is hosted at [ImMrMeeseeks.com](http://ImMrMeeseeks.com) using Amazon S3 and Cloudwatch.

I haven't exactly been following proper release branching processes...but in a perfect world I would have been pushing to the master branch every time I released.  Currently basically everything is happening in develop.

### History ###
Ignoring the fact that an entire angular app was totally unnecessary for this purpose...there's some other weird stuff in the code too.  Why are we using angular ui-router if there is only one state?  Well, like I mentioned, the main purpose here was to build an angular boilerplate.  Chances are, apps in the future will need states.  So, there was a version that involved another state, (doing some GIS stuff) but that has been commented out for now.  


### Setup ###

Development:

npm install

bower install

grunt serve

To package:

grunt